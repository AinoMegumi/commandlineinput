## 日本語
このInputクラスを使用する場合、InputクラスだけでなくStringInputクラスとNumInputクラスもコピーする必要があります。
そうしないとこのクラスは残り二つのクラスを継承しているため、コンパイルエラーになります。

## English
If you use this "Input" class, you need not only to copy this but also "StringInput" class and "NumInput" class.
Otherwise your source code fails to compile because this inherits others.

## 中國
如果你想用這個“Input”類，你還需要複製這個不僅是“StringInput”級和“NumInput”級。
因為這個類繼承的類的剩下的兩個，否則將會編譯錯誤。

## 한국
이 "Input"클래스를 사용하는 경우, 이것은뿐만 아니라 "StringInput"클래스 "NumInput"클래스도 복사해야합니다.
그렇지 않으면이 클래스는 나머지 두 클래스를 상속하고 있기 때문에 컴파일 오류가 발생합니다.

## русский
Если вы хотите использовать этот класс "Input", вы также должны скопировать этот класс не только "StringInput" и класс "NumInput".
Поскольку этот класс наследует остальные два класса, это будет ошибка компиляции иначе.

## Latinae
Si vis uti "Input" genere, et hoc non solum imitere oportet "StringInput" classis "NumInput" class.
Quod genus hoc genere duabus hereditario eam compilare error sit.

## italiano
Se si desidera utilizzare questa classe "Input", è inoltre necessario copiare questo non solo la classe "StringInput" e la classe "NumInput".
Poiché questa classe eredita i restanti due della classe, si compilerà errore altrimenti.

## français
Si vous voulez utiliser cette classe "Input", vous devez également copier ce non seulement la classe "StringInput" et la classe "NumInput".
Parce que cette classe hérite les deux autres de la classe, il compilera erreur autrement.

## Deutsch
Wenn Sie diesen "Input" Klasse verwenden möchten, müssen Sie auch diese nicht nur "StringInput" Klasse zu kopieren und "NumInput" Klasse.
Da diese Klasse die verbleibenden zwei der Klasse erbt, wird es Fehler anders kompilieren.

## español
Si desea utilizar esta clase de "Input", también es necesario copiar esta clase no sólo "StringInput" y clase "NumInput".
Debido a que esta clase hereda los dos restantes de la clase, se compilará el error de lo contrario.

## português
Se você quiser usar essa classe "Input", também é necessário para copiar este não só classe "StringInput" e classe "NumInput".
Como essa classe herda os dois restantes da classe, ele irá compilar erro contrário.

## ελληνικά
Αν θέλετε να χρησιμοποιήσετε αυτήν την κατηγορία "Input", θα πρέπει επίσης να αντιγράψετε αυτό όχι μόνο την κατηγορία "StringInput" και την κατηγορία "NumInput".
Επειδή αυτή η κλάση κληρονομεί τα υπόλοιπα δύο της κατηγορίας, θα καταρτίσει το σφάλμα διαφορετικά.
