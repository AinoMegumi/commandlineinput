﻿import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Input extends NumInput {
    public long getnum(long max, long min) throws IOException, NumberFormatException {
        return this.numRetWhenInRange(this.getlnum(), max, min);
    }
    public int getnum(int max, int min) throws IOException, NumberFormatException {
        return (int)this.numRetWhenInRange(this.getlnum(), max, min);
    }
    public short getnum(short max, short min) throws IOException, NumberFormatException {
        return (short)this.numRetWhenInRange(getlnum(), max, min);
    }
    public double getnum(double max, double min) throws IOException, NumberFormatException {
        return this.numRetWhenInRange(getdnum(), max, min);
    }
    public float getnum(float max, float min) throws IOException, NumberFormatException {
        return (float)this.numRetWhenInRange(this.getdnum(), max, min);
    }
}

abstract class StringInput {
    private final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    public String getString() throws IOException {
        return this.input.readLine();
    }
}

abstract class NumInput extends StringInput {
    protected long getlnum() throws IOException, NumberFormatException {
        return Long.parseLong(this.getString());
    }
    protected double getdnum() throws IOException, NumberFormatException {
        return Double.parseDouble(this.getString());
    }
    protected long numRetWhenInRange(long num, long max, long min) throws IOException  {
        if (max < min) return this.numRetWhenInRange(num, min, max);
        if (num < min || max < num) throw new IOException("入力された値が設定されている最大値または最小値の範囲を超えています。");
        else return num;
    }
    protected double numRetWhenInRange(double num, double max, double min) throws IOException  {
        if (max < min) return this.numRetWhenInRange(num, min, max);
        if (num < min || max < num) throw new IOException("入力された値が設定されている最大値または最小値の範囲を超えています。");
        else return num;
    }
}
